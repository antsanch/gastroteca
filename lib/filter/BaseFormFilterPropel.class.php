<?php

/**
 * Project filter form base class.
 *
 * @package    gastroteca
 * @subpackage filter
 * @author     Inhenieria Biomedica, HU, UANL
 */
abstract class BaseFormFilterPropel extends sfFormFilterPropel
{
  public function setup()
  {
  }
}
