<?php

/**
 * Video filter form base class.
 *
 * @package    gastroteca
 * @subpackage filter
 * @author     Inhenieria Biomedica, HU, UANL
 */
abstract class BaseVideoFormFilter extends BaseFormFilterPropel
{
  public function setup()
  {
    $this->setWidgets(array(
      'path'        => new sfWidgetFormFilterInput(),
      'time'        => new sfWidgetFormFilterInput(),
      'width'       => new sfWidgetFormFilterInput(),
      'height'      => new sfWidgetFormFilterInput(),
      'name'        => new sfWidgetFormFilterInput(),
      'img_path'    => new sfWidgetFormFilterInput(),
      'description' => new sfWidgetFormFilterInput(),
      'tags'        => new sfWidgetFormFilterInput(),
      'sumary'      => new sfWidgetFormFilterInput(),
      'active'      => new sfWidgetFormChoice(array('choices' => array('' => 'yes or no', 1 => 'yes', 0 => 'no'))),
      'created_at'  => new sfWidgetFormFilterDate(array('from_date' => new sfWidgetFormDate(), 'to_date' => new sfWidgetFormDate())),
      'updated_at'  => new sfWidgetFormFilterDate(array('from_date' => new sfWidgetFormDate(), 'to_date' => new sfWidgetFormDate())),
    ));

    $this->setValidators(array(
      'path'        => new sfValidatorPass(array('required' => false)),
      'time'        => new sfValidatorPass(array('required' => false)),
      'width'       => new sfValidatorPass(array('required' => false)),
      'height'      => new sfValidatorPass(array('required' => false)),
      'name'        => new sfValidatorPass(array('required' => false)),
      'img_path'    => new sfValidatorPass(array('required' => false)),
      'description' => new sfValidatorPass(array('required' => false)),
      'tags'        => new sfValidatorPass(array('required' => false)),
      'sumary'      => new sfValidatorPass(array('required' => false)),
      'active'      => new sfValidatorChoice(array('required' => false, 'choices' => array('', 1, 0))),
      'created_at'  => new sfValidatorDateRange(array('required' => false, 'from_date' => new sfValidatorDate(array('required' => false)), 'to_date' => new sfValidatorDate(array('required' => false)))),
      'updated_at'  => new sfValidatorDateRange(array('required' => false, 'from_date' => new sfValidatorDate(array('required' => false)), 'to_date' => new sfValidatorDate(array('required' => false)))),
    ));

    $this->widgetSchema->setNameFormat('video_filters[%s]');

    $this->errorSchema = new sfValidatorErrorSchema($this->validatorSchema);

    parent::setup();
  }

  public function getModelName()
  {
    return 'Video';
  }

  public function getFields()
  {
    return array(
      'id'          => 'Number',
      'path'        => 'Text',
      'time'        => 'Text',
      'width'       => 'Text',
      'height'      => 'Text',
      'name'        => 'Text',
      'img_path'    => 'Text',
      'description' => 'Text',
      'tags'        => 'Text',
      'sumary'      => 'Text',
      'active'      => 'Boolean',
      'created_at'  => 'Date',
      'updated_at'  => 'Date',
    );
  }
}
