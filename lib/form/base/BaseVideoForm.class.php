<?php

/**
 * Video form base class.
 *
 * @method Video getObject() Returns the current form's model object
 *
 * @package    gastroteca
 * @subpackage form
 * @author     Inhenieria Biomedica, HU, UANL
 */
abstract class BaseVideoForm extends BaseFormPropel
{
  public function setup()
  {
    $this->setWidgets(array(
      'id'          => new sfWidgetFormInputHidden(),
      'path'        => new sfWidgetFormTextarea(),
      'time'        => new sfWidgetFormInputText(),
      'width'       => new sfWidgetFormInputText(),
      'height'      => new sfWidgetFormInputText(),
      'name'        => new sfWidgetFormInputText(),
      'img_path'    => new sfWidgetFormTextarea(),
      'description' => new sfWidgetFormTextarea(),
      'tags'        => new sfWidgetFormInputText(),
      'sumary'      => new sfWidgetFormTextarea(),
      'active'      => new sfWidgetFormInputCheckbox(),
      'created_at'  => new sfWidgetFormDateTime(),
      'updated_at'  => new sfWidgetFormDateTime(),
    ));

    $this->setValidators(array(
      'id'          => new sfValidatorChoice(array('choices' => array($this->getObject()->getId()), 'empty_value' => $this->getObject()->getId(), 'required' => false)),
      'path'        => new sfValidatorString(array('required' => false)),
      'time'        => new sfValidatorString(array('max_length' => 5, 'required' => false)),
      'width'       => new sfValidatorString(array('max_length' => 4, 'required' => false)),
      'height'      => new sfValidatorString(array('max_length' => 4, 'required' => false)),
      'name'        => new sfValidatorString(array('max_length' => 128, 'required' => false)),
      'img_path'    => new sfValidatorString(array('required' => false)),
      'description' => new sfValidatorString(array('required' => false)),
      'tags'        => new sfValidatorPass(array('required' => false)),
      'sumary'      => new sfValidatorString(array('required' => false)),
      'active'      => new sfValidatorBoolean(array('required' => false)),
      'created_at'  => new sfValidatorDateTime(array('required' => false)),
      'updated_at'  => new sfValidatorDateTime(array('required' => false)),
    ));

    $this->widgetSchema->setNameFormat('video[%s]');

    $this->errorSchema = new sfValidatorErrorSchema($this->validatorSchema);

    parent::setup();
  }

  public function getModelName()
  {
    return 'Video';
  }


}
