<?php

/**
 * Project form base class.
 *
 * @package    gastroteca
 * @subpackage form
 * @author     Inhenieria Biomedica, HU, UANL
 */
abstract class BaseFormPropel extends sfFormPropel
{
  public function setup()
  {
  }
}
