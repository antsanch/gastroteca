<?php

/**
 * Base project form.
 * 
 * @package    gastroteca
 * @subpackage form
 * @author     Inhenieria Biomedica, HU, UANL 
 * @version    SVN: $Id: BaseForm.class.php 20147 2009-07-13 11:46:57Z FabianLange $
 */
class BaseForm extends sfFormSymfony
{
}
