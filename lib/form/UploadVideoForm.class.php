<?php

/**
 * Video form.
 *
 * @package    gastroteca
 * @subpackage form
 * @author     Inhenieria Biomedica, HU, UANL
 */
class UploadVideoForm extends BaseVideoForm
{
  public function configure()
  {
  # Establece las propiedades de los widgets

    $this->setWidget('file', new sfWidgetFormInputFile());
    $this->widgetSchema['img_path'] = new sfWidgetFormInputFile();

    $this->widgetSchema['tags'] = new sfWidgetFormTextarea();

        $this->getWidget('name')->setAttribute('placeholder', 'Nombre para identificar el video');
    $this->getWidget('description')->setAttribute('placeholder', 'Descripción breve del video y de lo que se observa');
    $this->getWidget('time')->setAttribute('placeholder', 'En segundos');
    $this->getWidget('width')->setAttribute('placeholder', 'En pixeles');
    $this->getWidget('height')->setAttribute('placeholder', 'En pixeles');
    $this->getWidget('tags')->setAttribute('placeholder', 'Serie de términos que ayudan a clasificar el video');

  # Establece las etiquetas de los campos
    $this->getWidget('file')->setLabel('Video:');

    $this->widgetSchema->setLabels(array(
      'path'        =>  'Dirección:',
      'time'        =>  'Duración:',
      'width'       =>  'Ancho:',
      'height'      =>  'Alto:',
      'name'        =>  'Nombre:',
      'img_path'    =>  'Imagen:',
      'description' =>  'Descripción:',
      'tags'        =>  'Etiquetas:',
      'active'      =>  'Activo:'
    ));

    $this->validatorSchema['file'] = new sfValidatorFile(array(), array(
      'required'  => 'Se necesita un archivo para cargar'
    ));

    $this->getValidator('name')
      ->setOption('required', true)
      ->setMessage('required', 'Se necesita el nombre del video');

    $this->getValidator('description')
      ->setOption('required', true)
      ->setMessage('required', 'Se necesita una breve descripcion, puede ser el mismo nombre del video');

    $this->validatorSchema['img_path'] = new sfValidatorFile(
      array(
        'required'  => false
      ),
      array(
        'required'  =>  'Se necesita especificar una imagen para representar el video.'
      )
    );

  }
}


/*

/**/
