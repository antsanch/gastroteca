#Proyecto de gastroteca

Primero nos cambiamos a nuestra carpeta de proyectos, en el ejemplo es webapps, pero puedes usar la que desees

    cd ~/webapps

Luego clonamos el repositorio a la computadora

    git clone https://antsanch@bitbucket.org/antsanch/gastroteca.git

Nos cambiamos a la carpeta recién descargada

    cd gastroteca

Actualizar los submodulos

    git submodule update --init --recursive

Copiar los archivo de configuración

    cp config/databases.yml.base config/databases.yml
    cp config/propel.ini.base config/propel.ini

Abre el archivo 'config/databases.yml' y configura el usuario y el password del servidor MySQL

    nano config/databases.yml

Haz lo mismo con el archivo 'config/propel.ini' y configura el usuario y el password del servidor MySQL

    nano config/propel.ini

Asegurarse de que los permisos de archivo sean los correctos

    php symfony project:permissions

Configurar el servidor apache segun la version disponible 2.2 o 2.4, en este ejemplo usamos el nombre 'gastroteca.lcl' pero puedes usar el que desees.
Debes cambiar la ruta '/home/antsanch/webapps/' por la ruta que uses en tu configuracion, puedes usar **search & replace** en casi cualquier editor de texto.

    <VirtualHost *:80>
    DocumentRoot /home/antsanch/webapps/gastroteca/web
    ServerName gastroteca.lcl

    <Directory "/home/antsanch/webapps/gastroteca/web">
    # Usa el siguiente comando para permitir accesos en apache 2.2 o inferior
    #  allow from all
    # Usa el siguiente comando para permitir accesos en apache 2.4 o superior
    #  Require all granted
      Options +Indexes
    </Directory>

    Alias /sf "/home/antsanch/webapps/gastroteca/lib/vendor/symfony/data/web/sf"
    <Directory "/home/antsanch/webapps/gastroteca/lib/vendor/symfony/data/web/sf">
    # Usa el siguiente comando para permitir accesos en apache 2.2 o inferior
    #  allow from all
    # Usa el siguiente comando para permitir accesos en apache 2.4 o superior
    #  Require all granted
      AllowOverride All
    </Directory>
    </VirtualHost>

Configurar el archivo de '/etc/hosts' para poder acceder al proyecto

    sudo nano /etc/hosts

Agregar la nueva dirección del proyecto, si no es el mismo equipo (localhost) cambia la IP por la correcta

    IP de servidor      Nombre de maquina
    127.0.0.1           gastroteca.lcl

Crea la base de datos en MySQL

Crea los datos del schema y alimenta la base de datos

    php symfony propel:build --sql --db

Acepta los cambios y ya esta, ahora puedes accesar al proyecto en: http://gastroteca.lcl

Con eso debe quedar listo :)