<?php

/**
 * web actions.
 *
 * @package    gastroteca
 * @subpackage web
 * @author     Inhenieria Biomedica, HU, UANL
 * @version    SVN: $Id: actions.class.php 23810 2009-11-12 11:07:44Z Kris.Wallsmith $
 */
class webActions extends sfActions
{
 /**
  * Executes index action
  *
  * @param sfRequest $request A request object
  */
  public function executeIndex(sfWebRequest $request)
  {

  }

  public function executeAcerca(sfWebRequest $request)
  {

  }
}
