<h1>Gastroteca Documentación</h1>

<p>Videoteca de gastroenterologia con casos clinicos y preguntas y respuestas</p>

<p>Se necesita adaptar el template a las necesidades del sitio</p>

<p>Para probar los modos del template visita:

<a href="http://www.keenthemes.com/preview/index.php?theme=metronic">Metronic Live Preview</a>
</p>

<h2>Lista de Modulos generados y opciones</h2>
<ul>
  <li>[22/Sep/2014] <?php echo link_to('Creacion del modulo WEB', 'web/index') ?> - Modulo estatico para colocar textos descriptivos</li>
  <li>[18/Sep/2014] <?php echo link_to('Identificar propiedades de videos', 'video/properties') ?> - Pruebas para leer las propiedades del video</li>
  <li>[18/Sep/2014] <?php echo link_to('Captura de errores', 'video/new') ?> - Captura de errores al enviar un formulario vacio</li>
  <li>[17/Sep/2014] <?php echo link_to('Carga de videos', 'video/new') ?> - En esta accion ya se configuro el template!!</li>
  <li>[15/Sep/2014] <?php echo link_to('Administración de videos', 'video/index') ?> </li>
  <li>[12/Sep/2014] <?php echo link_to('Módulo temporal de Login', 'sfGuardAuth/index') ?> </li>
  <li>[11/Sep/2014] <?php echo link_to('Documentacion', 'docs/index') ?> </li>
</ul>
