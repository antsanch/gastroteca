<?php use_stylesheets_for_form($form) ?>
<?php use_javascripts_for_form($form) ?>

<form class="form-horizontal" role="form" action="<?php echo url_for('video/'.($form->getObject()->isNew() ? 'create' : 'update').(!$form->getObject()->isNew() ? '?id='.$form->getObject()->getId() : '')) ?>" method="post" <?php $form->isMultipart() and print 'enctype="multipart/form-data" ' ?>>
<?php if (!$form->getObject()->isNew()): ?>
<input type="hidden" name="sf_method" value="put" />
<?php endif; ?>

  <div class="form-body">
    <?php if($form->hasGlobalErrors()): ?>
      <div class="alert alert-danger">
        <?php echo $form->renderGlobalErrors() ?>
      </div>
    <?php endif; ?>

  <!--  Cada uno de los campos, su etiqueta y los mensajes van en un <div class="form-group"> -->
        <div class="form-group <?php if ($form['file']->hasError()) echo 'has-error' ?>">
    <!--  La forma de asignar clases a un label es agregar una array como segundo parametro con la clave 'class'-->
          <?php echo $form['file']->renderLabel(null, array('class' => 'col-md-2 control-label')) ?>
          <div class="col-md-10">
      <!--  Para asignar atributos a un campo se envia un array con el formato 'attributo' => 'valor'-->
            <?php echo $form['file']->render(array('class' => 'form-control')) ?>
      <!--  Los mensajjes de error y ayudas se colocan en un <span class='help-block'> -->
            <span class="help-block">
              <?php echo ($form['file']->hasError()) ? $form['file']->renderError() : 'Elije el archivo de video que se cargara al sistema' ?>
            </span>
          </div>
        </div>

        <div class="form-group <?php if ($form['file']->hasError()) echo 'has-error' ?>">
          <?php echo $form['name']->renderLabel(null, array('class' => 'col-md-2 control-label')) ?>
          <div class="col-md-10 col-sm-10">
            <?php echo $form['name']->render(array('class' => 'form-control')) ?>
            <span class="help-block"><?php if ($form['name']->hasError()) echo $form['name']->renderError() ?></span>
          </div>
        </div>

        <div class="form-group <?php if ($form['description']->hasError()) echo 'has-error' ?>">
          <?php echo $form['description']->renderLabel(null, array('class' => 'col-md-2 control-label')) ?>
          <div class="col-md-10 col-sm-10">
            <?php echo $form['description']->render(array('class' => 'form-control')) ?>
            <span class="help-block"><?php echo $form['description']->renderError() ?></span>
          </div>
        </div>

        <div class="form-group">
          <?php echo $form['time']->renderLabel(null, array('class' => 'col-md-2 control-label')) ?>
          <div class="col-md-10 col-sm-10">
            <?php echo $form['time']->render(array('class' => 'form-control')) ?>
            <?php echo $form['time']->renderError() ?>
          </div>
        </div>

        <div class="form-group">
          <?php echo $form['width']->renderLabel(null, array('class' => 'col-md-2 control-label')) ?>
          <div class="col-md-10 col-sm-10">
            <?php echo $form['width']->render(array('class' => 'form-control')) ?>
            <?php echo $form['width']->renderError() ?>
          </div>
        </div>

        <div class="form-group">
          <?php echo $form['height']->renderLabel(null, array('class' => 'col-md-2 control-label')) ?>
          <div class="col-md-10 col-sm-10">
            <?php echo $form['height']->render(array('class' => 'form-control')) ?>
            <?php echo $form['height']->renderError() ?>
          </div>
        </div>

        <div class="form-group <?php if ($form['img_path']->hasError()) echo 'has-error' ?>">
          <?php echo $form['img_path']->renderLabel(null, array('class' => 'col-md-2 control-label')) ?>
          <div class="col-md-10 col-sm-10">
            <?php echo $form['img_path']->render(array('class' => 'form-control')) ?>
            <span class="help-block">
              <?php echo ($form['img_path']->hasError()) ? $form['img_path']->renderError() : 'Imagen que se usará como representación del video.' ?>
            </span>
          </div>
        </div>

        <div class="form-group">
          <?php echo $form['tags']->renderLabel(null, array('class' => 'col-md-2 control-label')) ?>
          <div class="col-md-10 col-sm-10">
            <?php echo $form['tags']->render(array('class' => 'form-control')) ?>
            <span class="help-block">Lista de etiquetas separadas por comas<?php echo $form['tags']->renderError() ?></span>
          </div>
        </div>

        <div class="form-group">
          <?php echo $form['active']->renderLabel(null, array('class' => 'col-md-2 control-label')) ?>
          <div class="col-md-10 col-sm-10">
            <?php echo $form['active']->render(array('class' => 'form-control')) ?>
            <span class="help-block">Especifíca si el video estará visible o no</span>
          </div>
        </div>

      <div class="form-actions right">
        <?php echo $form->renderHiddenFields(false) ?>
        &nbsp;<a href="<?php echo url_for('video/index') ?>" class="btn btn-sm default"><i class="fa fa-reply"></i> Regresar</a>
        <?php if (!$form->getObject()->isNew()): ?>
          &nbsp;<?php echo link_to('<i class="fa fa-times"></i> Eliminar', 'video/delete?id='.$form->getObject()->getId(), array('method' => 'delete', 'confirm' => 'Are you sure?', 'class' => 'btn btn-sm red')) ?>
        <?php endif; ?>
        <button class="btn btn-sm green" type="submit"><i class="fa fa-upload"></i> Cargar</button>
      </div>

  </div>
</form>
