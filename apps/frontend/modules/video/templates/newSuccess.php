<div class="col-md-9 col-sm-9">

  <div class="portlet box green">
    <div class="portlet-title">
      <div class="caption">
        <i class="fa fa-upload"></i>Cargar Video
      </div>
      <!--
      <div class="tools">
        <a href="" class="collapse">
        </a>
        <a href="#portlet-config" data-toggle="modal" class="config">
        </a>
        <a href="" class="reload">
        </a>
        <a href="" class="remove">
        </a>
      </div> -->
    </div>
    <div class="portlet-body form">
<?php include_partial('form', array('form' => $form)) ?>
    </div>
  </div>
</div>
