<h1>Videos List</h1>

<table>
  <thead>
    <tr>
      <th>Id</th>
      <th>Path</th>
      <th>Time</th>
      <th>Width</th>
      <th>Height</th>
      <th>Name</th>
      <th>Description</th>
      <th>Tags</th>
      <th>Sumary</th>
      <th>Active</th>
      <th>Created at</th>
      <th>Updated at</th>
    </tr>
  </thead>
  <tbody>
    <?php foreach ($Videos as $Video): ?>
    <tr>
      <td><a href="<?php echo url_for('video/edit?id='.$Video->getId()) ?>"><?php echo $Video->getId() ?></a></td>
      <td><?php echo $Video->getPath() ?></td>
      <td><?php echo $Video->getTime() ?></td>
      <td><?php echo $Video->getWidth() ?></td>
      <td><?php echo $Video->getHeight() ?></td>
      <td><?php echo $Video->getName() ?></td>
      <td><?php echo $Video->getDescription() ?></td>
      <td><?php echo $Video->getTags() ?></td>
      <td><?php echo $Video->getSumary() ?></td>
      <td><?php echo $Video->getActive() ?></td>
      <td><?php echo $Video->getCreatedAt() ?></td>
      <td><?php echo $Video->getUpdatedAt() ?></td>
    </tr>
    <?php endforeach; ?>
  </tbody>
</table>

  <a href="<?php echo url_for('video/new') ?>">New</a>
