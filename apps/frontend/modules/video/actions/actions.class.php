<?php

/**
 * video actions.
 *
 * @package    gastroteca
 * @subpackage video
 * @author     Inhenieria Biomedica, HU, UANL
 */
class videoActions extends sfActions
{
  public function executeIndex(sfWebRequest $request)
  {
    $this->Videos = VideoQuery::create()->find();
  }

  public function executeNew(sfWebRequest $request)
  {
    $this->form = new UploadVideoForm();
  }

  public function executeProperties(sfWebRequest $request)
  {

  }

  public function executeCreate(sfWebRequest $request)
  {
    $this->forward404Unless($request->isMethod(sfRequest::POST));
    $this->form = new UploadVideoForm();
    $this->processForm($request, $this->form);
    $this->setTemplate('new');
  }

  public function executeEdit(sfWebRequest $request)
  {
    $Video = VideoQuery::create()->findPk($request->getParameter('id'));
    $this->forward404Unless($Video, sprintf('Object Video does not exist (%s).', $request->getParameter('id')));
    $this->form = new VideoForm($Video);
  }

  public function executeUpdate(sfWebRequest $request)
  {
    $this->forward404Unless($request->isMethod(sfRequest::POST) || $request->isMethod(sfRequest::PUT));
    $Video = VideoQuery::create()->findPk($request->getParameter('id'));
    $this->forward404Unless($Video, sprintf('Object Video does not exist (%s).', $request->getParameter('id')));
    $this->form = new VideoForm($Video);

    $this->processForm($request, $this->form);

    $this->setTemplate('edit');
  }

  public function executeDelete(sfWebRequest $request)
  {
    $request->checkCSRFProtection();

    $Video = VideoQuery::create()->findPk($request->getParameter('id'));
    $this->forward404Unless($Video, sprintf('Object Video does not exist (%s).', $request->getParameter('id')));
    $Video->delete();

    $this->redirect('video/index');
  }

  protected function processForm(sfWebRequest $request, sfForm $form)
  {
    $form->bind($request->getParameter($form->getName()), $request->getFiles($form->getName()));
    if ($form->isValid())
    {
      $Video = $form->save();

      $this->redirect('video/edit?id='.$Video->getId());
    }
  }
}
