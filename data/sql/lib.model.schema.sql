
# This is a fix for InnoDB in MySQL >= 4.1.x
# It "suspends judgement" for fkey relationships until are tables are set.
SET FOREIGN_KEY_CHECKS = 0;

-- ---------------------------------------------------------------------
-- gtc_video
-- ---------------------------------------------------------------------

DROP TABLE IF EXISTS `gtc_video`;

CREATE TABLE `gtc_video`
(
    `id` INTEGER NOT NULL AUTO_INCREMENT,
    `url` TEXT,
    `time` VARCHAR(5),
    `width` VARCHAR(4),
    `height` VARCHAR(4),
    `name` VARCHAR(128),
    `description` TEXT,
    `sumary` TEXT,
    `active` TINYINT(1),
    `created_at` DATETIME,
    `updated_at` DATETIME,
    PRIMARY KEY (`id`)
) ENGINE=InnoDB;

# This restores the fkey checks, after having unset them earlier
SET FOREIGN_KEY_CHECKS = 1;
